<?php

require_once 'vendor/autoload.php';
require_once 'HtmlParser.php';

$parser = new HtmlParser();
$parser->execute($argv[1] ?? null);
