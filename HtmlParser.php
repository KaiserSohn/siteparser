<?php

use PHPHtmlParser\Dom;
use PHPHtmlParser\Exceptions\ChildNotFoundException;
use PHPHtmlParser\Exceptions\CircularException;
use PHPHtmlParser\Exceptions\ContentLengthException;
use PHPHtmlParser\Exceptions\LogicalException;
use PHPHtmlParser\Exceptions\NotLoadedException;
use PHPHtmlParser\Exceptions\StrictException;
use Psr\Http\Client\ClientExceptionInterface;

class HtmlParser
{
    private const JSON_FILE_NAME = '{date}_parsed_data.json';

    private const SOURCE = 'https://mebelino-mebel.ru';

    /**
     * @var Dom
     */
    private $dom;

    /**
     * @var array
     */
    private $categories = [];

    public function __construct()
    {
        $this->dom = new Dom();
    }

    /**
     * @param $path
     * @throws ChildNotFoundException
     * @throws CircularException
     * @throws ContentLengthException
     * @throws LogicalException
     * @throws StrictException
     * @throws ClientExceptionInterface
     */
    private function loadFromUrl($path = '/')
    {
        $this->dom->loadFromUrl(str_replace('//', '/', static::SOURCE . '/' . $path));
    }

    /**
     * @param string $path
     * @return Dom
     * @throws ChildNotFoundException
     * @throws CircularException
     * @throws ClientExceptionInterface
     * @throws ContentLengthException
     * @throws LogicalException
     * @throws StrictException
     */
    private function getInstanceOfDom($path = '/')
    {
        $dom = new Dom();
        $dom->loadFromUrl(str_replace('//', '/', static::SOURCE . '/' . $path));
        return $dom;
    }

    /**
     * @throws ChildNotFoundException
     * @throws CircularException
     * @throws ClientExceptionInterface
     * @throws ContentLengthException
     * @throws LogicalException
     * @throws StrictException
     * @throws NotLoadedException
     */
    private function parseCategories()
    {
        $this->loadFromUrl();

        /** @var Dom\Node\Collection $categories */
        $this->dom->find('.widget_nav li a')->each(function (Dom\Node\HtmlNode $node) {
            /** @var Dom\Node\HtmlNode $nameSelector */
            $nameSelector = $node->find('span')[0];
            /** @var Dom\Node\Collection $childrenCategoriesSelector */
            $childrenCategoriesSelector = $node->parent->find('li a');

            if (!$nameSelector) {
                return;
            }

            $childrenCategories = array_map(function (Dom\Node\HtmlNode $node) {
                return [
                    'name' => $node->innerHtml(),
                    'href' => $node->getAttribute('href'),
                ];
            }, $childrenCategoriesSelector->toArray());


            $this->categories[] = [
                'name' => $nameSelector->innerHtml(),
                'href' => $node->getAttribute('href'),
                'children' => $childrenCategories,
            ];
        });
    }

    /**
     * @param $category
     * @throws ChildNotFoundException
     * @throws CircularException
     * @throws ClientExceptionInterface
     * @throws ContentLengthException
     * @throws LogicalException
     * @throws NotLoadedException
     * @throws StrictException
     */
    private function parsePaginatedItems(&$category)
    {
        $navDomElementsCount = $this->dom->find('ul.blog__nav a')->count();
        $pagesCount = $navDomElementsCount > 0 ? $navDomElementsCount : 1;

        for ($i = 1; $i <= $pagesCount; $i++) {
            if ($i > 1) {
                $pageQuery = http_build_query([
                    'page' => $i,
                ]);

                $this->loadFromUrl($category['href'] . '?' .  $pageQuery);
            }

            $this->dom->find('.portfolio__item-inner')->each(function (Dom\Node\HtmlNode $node) use (&$category) {
                /**
                 * @var Dom\Node\HtmlNode $titleSelector
                 * @var Dom\Node\HtmlNode $hrefSelector
                 * @var Dom\Node\HtmlNode $imageSelector
                 */
                $titleSelector = $node->find('.portfolio__item-title')[0];
                $hrefSelector = $node->find('a.portfolio__link')[0];
                $imageSelector = $node->find('img')[0];

                $detailDom = $this->getInstanceOfDom($hrefSelector->getAttribute('href'));
                $detailImageSelector = $detailDom->find('img.minifot')[0];

                $item = [
                    'title' => strip_tags($titleSelector->innerHtml()),
                    'href' => $hrefSelector->getAttribute('href'),
                    'image' => [
                        'src' =>  static::SOURCE . '/' . $imageSelector->getAttribute('src'),
                        'alt' => $imageSelector->getAttribute('alt'),
                    ],
                ];

                if ($detailImageSelector) {
                    $item['detail_image'] = [
                        'src' =>  static::SOURCE . '/' . $detailImageSelector->getAttribute('src'),
                        'alt' => $detailImageSelector->getAttribute('alt'),
                    ];
                }

                $category['items'][] = $item;
            });
        }
    }

    /**
     * @param $category
     * @throws ChildNotFoundException
     * @throws CircularException
     * @throws ClientExceptionInterface
     * @throws ContentLengthException
     * @throws LogicalException
     * @throws NotLoadedException
     * @throws StrictException
     */
    private function parseCategory(&$category)
    {
        $this->loadFromUrl($category['href']);

        $thirdLevelSections = $this->dom->find('.content .catalog.row a.catalog__item.column.column_three');

        if ($thirdLevelSections->count() > 0) {
            $thirdLevelSections->each(function (Dom\Node\HtmlNode $node) use (&$category) {
                $this->loadFromUrl($node->getAttribute('href'));
                $name = $node->find('span')[0];

                if (!$name) {
                    return;
                }

                $childCategory = [
                    'name' => $name->innerHtml(),
                    'href' => $node->getAttribute('href'),
                ];

                $this->parsePaginatedItems($childCategory);

                $category['children'][] = $childCategory;
            });
        } else {
            $this->parsePaginatedItems($category);
        }
    }

    /**
     * @throws ChildNotFoundException
     * @throws CircularException
     * @throws ClientExceptionInterface
     * @throws ContentLengthException
     * @throws LogicalException
     * @throws NotLoadedException
     * @throws StrictException
     */
    private function fillItems()
    {
        foreach ($this->categories as &$category) {
            if ($category['href'] !== '/peregorodki-na-zakaz.html') {
                continue;
            }

            $this->parseCategory($category);

            foreach ($category['children'] as &$child) {
                $this->parseCategory($child);
            }
        }
    }

    public function fillJson($fileName)
    {
        if (!$fileName) {
            $fileName = strtr(static::JSON_FILE_NAME, [
                '{date}' => date('d_m_Y_H_i_s'),
            ]);
        }

        file_put_contents($fileName, json_encode($this->categories));
    }

    /**
     * @param null $fileName
     * @throws ChildNotFoundException
     * @throws CircularException
     * @throws ClientExceptionInterface
     * @throws ContentLengthException
     * @throws LogicalException
     * @throws NotLoadedException
     * @throws StrictException
     */
    public function execute($fileName = null)
    {
        echo date('d.m.Y H:i:s') . PHP_EOL;
        $this->parseCategories();
        $this->fillItems();
        $this->fillJson($fileName);
        echo (memory_get_usage() / 8 / 1024 / 1024) . 'Mb' . PHP_EOL;
        echo date('d.m.Y H:i:s') . PHP_EOL;
    }
}
